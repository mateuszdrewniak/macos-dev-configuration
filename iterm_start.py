#!/usr/bin/env python3.7

import iterm2

# |=======================|
# |        CONFIG         |
# |=======================|

# Decides whether this script runs forever in the background or quits after 
# opening the tabs for the first time
RUN_FOREVER = False

# The profile which triggers tab creation
TRIGGER_PROFILE_NAME = 'Hotkey Window'

ITERM_TABS = [
    {
        'title': 'cli',             # set tab title
        'color': [255, 85, 85],    # customize RGB colors of tabs
    },
    {
        'title': 'irb',
        'color': [241, 250, 140],
    },
    {
        'title': 'server',
        'color': [189, 147, 249],
        'pane_split': {
            'vertical': True,
            'pane_split': {
                'vertical': False,
                'pane_split': {
                    'vertical': False,
                }
            }
        }
    }
]

# |=======================|
# |      END CONFIG       |
# |=======================|

async def split_pane(session, vertical, before):
    bool_before = before == True
    return await session.async_split_pane(vertical = vertical, before = bool_before)

async def set_tab_color(color, session):
    change = iterm2.LocalWriteOnlyProfile()
    change.set_tab_color(color)
    change.set_use_tab_color(True)
    await session.async_set_profile_properties(change)

async def create_tab(window, config, session = None):
    if session is None:
        await window.async_create_tab()
        session = window.current_tab.current_session

    color = iterm2.Color(config['color'][0], config['color'][1], config['color'][2])
    await set_tab_color(color, session)
    await window.current_tab.async_set_title(config['title'])

    pane_split = config.get('pane_split')
    current_session = session

    if not pane_split:
        return
    
    while True:
        current_session = await split_pane(current_session, pane_split.get('vertical'), pane_split.get('before'))
        pane_split = pane_split.get('pane_split')
        
        if not pane_split:
            break

async def open_work_tabs(window):
    first_session = session = window.current_tab.current_session

    for i, config in enumerate(ITERM_TABS):
        if i == 1:
            session = None
        await create_tab(window, config, session)

    await first_session.async_activate()

async def main(connection):
    app = await iterm2.async_get_app(connection)
    async with iterm2.NewSessionMonitor(connection) as mon:
        while True:
            session_id = await mon.async_get()
            session = app.get_session_by_id(session_id)
            if session:
                profile = await session.async_get_profile()
                if profile.name == TRIGGER_PROFILE_NAME:
                    await open_work_tabs(app.current_terminal_window)
                    if not RUN_FOREVER:
                        break


iterm2.run_forever(main) if RUN_FOREVER else iterm2.run_until_complete(main)
