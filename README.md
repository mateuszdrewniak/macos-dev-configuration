MacOS setup for development. Mainly focused on Ruby on Rails development, with the addition of Python and Node.js.

[[_TOC_]]

# Prerequisites

Any following chapter will require you to finish these steps here first

1. Open the Terminal app.

1. Install Xcode CLI tools ⚙️

   ```sh
   $ xcode-select --install
   ```

1. Install homebrew 🍺  (https://brew.sh/)

   ```sh
   $ /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"
   ```

1. Install ZSH (an alternative to BASH)

   ```sh
   $ brew install zsh
   ```

1. Install Oh My Zsh (https://github.com/ohmyzsh/ohmyzsh)

   ```sh
   $ sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
   ```

1. Install Visual Studio Code 😎 (if you haven't already)

   ```sh
   $ brew install --cask visual-studio-code
   ```

1. Enable Visual Studio Code CLI

   - Open Visual Studio Code

      ```sh
      $ open /Applications/Visual\ Studio\ Code.app
      ```

   - Press `Shift + Command + P`
   - Type `install 'code'` in the prompt that opens and press `Enter/Return`
       ![VSCode CLI](_images_/vscode-install-cli.png)

1. Download and install the FiraCode font (https://github.com/tonsky/FiraCode). Just open and click install on all .ttf files inside the downloaded zip archive.

1. Install GPG Suite

   ```sh
   $ brew install --cask gpg-suite
   ```

1. Restart your Terminal

   ```sh
   $ exec $SHELL
   ```

1. Install fzf (fuzzy finder)

   ```sh
   $ brew install fzf
   $ $(brew --prefix)/opt/fzf/install
   ```

1. Install `zoxide` (better `cd` command)

   ```sh
   $ brew install zoxide
   $ echo 'eval "$(zoxide init zsh)"' >> ~/.zshrc
   ```

   ![bat](_images_/zoxide.gif)

1. install `bat` (pretty `cat` replacement)

   ```sh
   $ brew install bat
   ```

   ![bat](_images_/bat.jpeg)

1. install `eza` (pretty `ls` replacement)

   ```sh
   $ brew install eza
   ```

   ![eza](_images_/eza.jpg)

1. Add the required aliases to your `~/.zshrc` file.

   ```sh
   $ curl https://gitlab.com/mateuszdrewniak/vscode-configuration/-/raw/master/zshrc-example >> ~/.zshrc
   ```

# Import my VSCode config

Feel free to skip this part.

Please make sure that the steps from the `Prerequisites` chapter have been carried out before following the instructions below.

1. Import the VSCode extensions by running:

   ```sh
   $ curl https://gitlab.com/mateuszdrewniak/vscode-configuration/-/raw/master/import_extensions.sh | bash
   ```

1. Import the VSCode config

   ```sh
   $ curl https://gitlab.com/mateuszdrewniak/vscode-configuration/-/raw/master/settings.json > ~/Library/Application\ Support/Code/User/settings.json
   ```

# Configure your terminal

Please make sure that the steps from the `Prerequisites` chapter have been carried out before following the instructions below.

1. Install iTerm2 (a much better Terminal for MacOS)

   ```sh
   $ brew install --cask iterm2
   ```

1. Close the Terminal app and from now on use iTerm2

   ```sh
   $ open /Applications/iTerm.app
   ```

1. Install a nerdfont of your liking (https://www.nerdfonts.com/font-downloads) - i suggest the Termines Nerd Font. Just open and click install on all downloaded .ttf files

1. Configure iTerm2:

   - Open the iTerm2 preferences

      ![iTerm Preferences](_images_/iterm-preferences.png)

   - Set Tab location to Bottom

      ![iTerm set Tab to Bottom](_images_/iterm-set-tab-bottom.png)

   - Enable the iTerm2 Python API

      ![iTerm Python API](_images_/iterm-enable-api.png)

   - Install the iTerm2 Python Runtime

      ![iTerm Python Runtime](_images_/iterm-python-runtime.png)

   - Create a Hotkey Windows

      ![iTerm Hotkey](_images_/iterm-hotkey.png)

   - Define a hotkey which will open and close the terminal Hotkey Window (I use `§`)

      ![iTerm Set Hotkey](_images_/iterm-set-hotkey.png)

   - Choose the NerdFont you installed in previous steps as the terminal's main font

      ![iTerm Font Default](_images_/iterm-configure-default.png)

   - Do the same with the new Hotkey Window profile

      ![iTerm Font Hotkey Window](_images_/iterm-configure-hotkey-window.png)

   - Customise the style of the hotkey window

      ![iTerm Hotkey Window Style](_images_/iterm-set-hotkey-window-style.png)

   - Customise the style of the default window

      ![iTerm Hotkey Window Style](_images_/iterm-set-default-window-style.png)

1. Create the AutoLaunch script directory for iTerm2

   ```sh
   $ mkdir ~/Library/Application\ Support/iTerm2/Scripts/AutoLaunch
   ```

1. Create the iTerm startup script:

   ```sh
   $ curl https://gitlab.com/mateuszdrewniak/vscode-configuration/-/raw/master/iterm_start.py > ~/Library/Application\ Support/iTerm2/Scripts/AutoLaunch/iterm_start.py
   ```

1. Install the powerlevel10k zsh theme (https://github.com/romkatv/powerlevel10k#oh-my-zsh)

   ```sh
   $ git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/themes/powerlevel10k
   ```

1. Set `ZSH_THEME="powerlevel10k/powerlevel10k"` in `~/.zshrc`

   ```sh
   $ vim ~/.zshrc
   # or
   $ nano ~/.zshrc
   # or
   $ code ~/.zshrc
   ```

1. Restart iTerm2

   ```sh
   $ exec $SHELL
   ```

# Languages

Please make sure that the steps from the `Prerequisites` chapter have been carried out before following the instructions below.

## Python

1. Install pyenv (Python version manager)

   ```sh
   $ brew install pyenv
   ```

1. Set up the pyenv in the shell

   ```sh
   $ echo 'export PYENV_ROOT="$HOME/.pyenv"' >> ~/.zshrc
   $ echo 'command -v pyenv >/dev/null || export PATH="$PYENV_ROOT/bin:$PATH"' >> ~/.zshrc
   $ echo 'eval "$(pyenv init -)"' >> ~/.zshrc
   ```

1. Restart iTerm2

1. Install a supported Python version (eg. `3.10.4`)

   ```sh
   $ pyenv install 3.10.4
   ```

1. Set the previously installed Python version as default

   ```sh
   $ pyenv global 3.10.4
   ```

1. Install Jupyter

   ```sh
   $ pip3 install notebook
   # or
   $ pip install notebook
   ```

## Ruby

### General

1. Install `rbenv` (https://github.com/rbenv/rbenv)

   ```sh
   $ brew install rbenv ruby-build
   ```

1. Add rbenv init to your `~/.zshrc`

   ```sh
   $ echo 'eval "$(rbenv init - zsh)"' >> ~/.zshrc
   ```

1. Restart your terminal

1. Install a supported and stable version of Ruby (eg. `3.1.2`)

   ```sh
   $ rbenv install 3.1.2
   ```

1. Set the aforementioned version of Ruby as the default one

   ```sh
   $ rbenv global 3.1.2
   ```

1. Install gems required for Visual Studio Code extensions

   ```sh
   $ gem install rubocop
   $ gem install solargraph
   ```

1. Prepare for Rails System Tests

   - Install Google Chrome (https://www.google.com/intl/pl/chrome/)
   - Install chromedriver

      ```sh
      $ brew install --cask chromedriver
      ```

### Jupyter

Please make sure to first configure Python with Jupyter on your system (chapter above)

1. Install IRuby dependencies

   ```sh
   $ brew install automake gmp libtool wget czmq
   $ brew install zeromq --HEAD
   ```

1. Install the IRuby kernel

   ```sh
   $ gem install ffi-rzmq
   $ gem install iruby --pre
   $ iruby register --force
   ```

## JavaScript

1. Install Node.js - https://nodejs.org/en/download/

1. Install NVM (Node Version Manager)

   ```sh
   $ curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/master/install.sh | bash
   ```

1. Restart iTerm

1. Install an LTS version of node through NVM.

   ```sh
   $ nvm install 'lts/*'
   ```

1. Set the latest version of node as the default one.

   ```sh
   $ nvm alias default 'lts/*'
   ```

1. Install yarn

   ```sh
   $ npm install --location=global yarn
   # or
   $ sudo npm install --location=global yarn
   ```

# Data Bases

Please make sure that the steps from the `Prerequisites` chapter have been carried out before following the instructions below.

## SQL

1. Install MySQL Server

   ```sh
   $ brew install mysql
   ```

1. Start MySQL service

   ```sh
   $ brew services start mysql
   ```

1. Install Sequel Ace (DB manager)

   ```sh
   $ brew install --cask sequel-ace
   ```

1. Configure Sequel Ace for local MySQL (the default password for `root` is an empty string!)

   ![Sequel Ace Config](_images_/sequel-ace-config.png)

## NoSQL

1. Install Redis

   ```sh
   $ brew install redis
   ```

1. Start Redis service

   ```sh
   $ brew services start redis
   ```

1. Install ElasticSearch

   - intel

      ```sh
      $ brew install elasticsearch
      ```

   - m1/arm

      ```sh
      $ brew install elasticsearch --build-from-source
      ```

1. Start ElasticSearch service

   ```sh
   $ brew services start elasticsearch
   ```

# Generate SSH and GPG keys

Do it only if you haven't got a valid GPG or SSH key already.

1. Generate a new SSH key (replace `your_email@example.com` with your real email!)

   ```sh
   $ ssh-keygen -t ed25519 -C "your_email@example.com"
   ```

1. Generate a new GPG key

   ```sh
   $ gpg --default-new-key-algo rsa4096 --gen-key
   ```

# Utilities

## Git client

- GitKraken (https://www.gitkraken.com/)

   ```sh
   $ brew install --cask gitkraken
   ```

- Sourcetree (https://www.sourcetreeapp.com/)

   ```sh
   $ brew install --cask sourcetree
   ```

## KeePassXC

A local encrypted password database (https://keepassxc.org/)

```sh
$ brew install --cask keepassxc
```

## Tunnelblick

Free software for OpenVPN

```sh
$ brew install --cask tunnelblick
```

## Thunderbird

An open source mail client
1. Install Thunderbird through homebrew

   ```sh
   $ brew install --cask thunderbird
   ```

1. Open Thunderbird and set it as the default mail client

   ```sh
   $ open /Applications/Thunderbird.app
   ```

1. Navigate to the Addons panel

   ![Thunderbird - Open Addons](_images_/thunderbird-open-addons.png)

1. Search for `markdown` in the addons store

   ![Thunderbird - Search for markdown](_images_/thunderbird-search-for-markdown.png)

1. Add the `Markdown Here Revival` addon to use markdown in your emails!

   ![Thunderbird - Install Markdown Here Revival](_images_/thunderbird-install-markdown-addon.png)

1. Export GPG keys to files

   ```sh
   $ gpg -a --export > ~/Desktop/gpg_public_keys.asc
   $ gpg -a --export-secret-keys > ~/Desktop/gpg_private_keys.asc
   ```

1. Open the PGP Manager

   ![Thunderbird - Open PGP Manager](_images_/thunderbird-open-pgp-manager.png)

1. Open the public key importer wizard

   ![Thunderbird - Import Public Keys](_images_/thunderbird-open-import-public-gpg.png)

1. Choose the public keys file

   ![Thunderbird - Choose Public Keys File](_images_/thunderbird-choose-public-gpg-keys.png)

1. Finish the public key import

   ![Thunderbird - Finish public keys import](_images_/thunderbird-accept-imported-public-keys.png)

1. Open the private key importer wizard

   ![Thunderbird - Import Private Keys](_images_/thunderbird-open-import-private-gpg.png)

1. Open the private key file selector

   ![Thunderbird - Open Private Key file selector](_images_/thunderbird-open-select-private-keys.png)

1. Choose the private keys file

   ![Thunderbird - Choose the private keys file](_images_/thunderbird-choose-private-gpg-keys.png)

1. Finish the private key import

   ![Thunderbird - Continue private key import](_images_/thunderbird-import-gpg-private-keys-continue.png)

1. Remove remaining GPG files

   ```sh
   $ rm ~/Desktop/gpg_public_keys.asc ~/Desktop/gpg_private_keys.asc
   ```

1. Open Thunderbird preferences

   ![Thunderbird - Open Preferences](_images_/thunderbird-open-preferences.jpg)

1. Enter account settings

   ![Thunderbird - Enter Account Settings](_images_/thunderbird-enter-account-settings.png)

1. Add a GPG key to your email

   ![Thunderbird - Add a GPG key to email](_images_/thunderbird-add-new-gpg-key.png)

1. Enable digital signature

   ![Thunderbird - Enable digital signature](_images_/thunderbird-enable-digital-signature.png)

## Spotify

Write your code with great music

```sh
$ brew install --cask spotify
```

## Vanilla

Menu bar icon manager (https://matthewpalmer.net/vanilla/)

## Rectangle

Move and resize windows in macOS using keyboard shortcuts or snap areas (https://rectangleapp.com/)

## Itsycal

A nice little calendar in the menu bar (https://www.mowglii.com/itsycal/)

## Kap

Record your screen with ease. Recordings can be exported as Gifs. (https://getkap.co/)

## ncdu

CLI disk usage analyzer.

```sh
$ brew install ncdu
```

![ncdu](_images_/ncdu.jpeg)

## btop

A great CLI resource monitor.

```sh
$ brew install btop
```

![btop](_images_/btop.jpeg)

# System config

1. Open System Preferences

   ![System Preferences](_images_/system-preferences.png)

1. Enter Users & Groups

   ![System Preferences Users](_images_/system-preferences-users.png)

1. Add the programmes you would like to open automatically when you log in (iTerm2, Mail etc.)

   ![System Preferences Users](_images_/system-preferences-add-startup.png)

1. Enter Security & Privacy in System Preferences

   ![System Preferences Get Back](_images_/system-preferences-get-back.png)

   ![System Preferences Security & Privacy](_images_/system-preferences-security-and-privacy.png)

1. Enable the firewall on your Mac

   ![System Preferences Enable the firewall](_images_/system-preferences-enable-firewall.png)

1. Enable FileVault (drive encryption)

   ![System Preferences Enable the firewall](_images_/system-preferences-enable-file-vault.png)

1. Open Finder

   ```sh
   $ open ~
   ```

1. Open Finder preferences

   ![Finder - Open preferences](_images_/finder-open-preferences.png)

1. Configure Finder general preferences

   ![Finder - General preferences](_images_/finder-general-prefs.png)

1. Configure Finder sidebar preferences

   ![Finder - Sidebar preferences](_images_/finder-sidebar-prefs.png)

1. Configure Finder advanced preferences

   ![Finder - Advanced preferences](_images_/finder-advanced-prefs.png)

# Migrate Keys, Certificates and VPNs from your old machine

- On the old machine

   1. Create the `Migration` folder on the desktop of your old machine

      ```sh
      $ mkdir ~/Desktop/Migration
      $ mkdir ~/Desktop/Migration/Certificates
      $ mkdir ~/Desktop/Migration/GPG
      ```

   1. Copy Tunnelblick configs to the `Migration` directory.

      ```sh
      $ cp -R ~/Library/Application\ Support/Tunnelblick/Configurations ~/Desktop/Migration/Tunnelblick/
      ```

   1. Copy the `.ssh` folder to the `Migration` directory

      ```sh
      $ cp -R ~/.ssh ~/Desktop/Migration/.ssh/
      ```

   1. Export Certificates from `Keychain Access` to the `~/Desktop/Migration/Certificates` directory

      ```sh
      $ open /System/Applications/Utilities/Keychain\ Access.app
      ```

      ![Keychain Access certificate export](_images_/keychain-access-export-certificates.png)

   1. Export public GPG keys to the `~/Desktop/Migration/GPG` directory

      ```sh
      $ gpg -a --export > ~/Desktop/Migration/GPG/public_keys.asc
      ```

   1. Export private GPG keys to the `~/Desktop/Migration/GPG` directory

      ```sh
      $ gpg -a --export-secret-keys > ~/Desktop/Migration/GPG/private_keys.asc
      ```

- On the new machine

  1. Copy the `~/Desktop/Migration` directory from the old machine over to your new machine as `~/Desktop/Migration`

  1. Copy the `Migration/.ssh` folder as `~/.ssh`

     ```sh
     $ cp -R ~/Desktop/Migration/.ssh ~/.ssh
     ```

  1. Import Certificates from `~/Desktop/Migration/Certificates`

      ```sh
      $ for FILE in ~/Desktop/Migration/Certificates/*; do open $FILE; done
      ```

  1. Import Tunnelblick configs from `~/Desktop/Migration/Tunnelblick`

      ```sh
      $ for FILE in ~/Desktop/Migration/Tunnelblick/*; do open $FILE; done
      ```

   1. Import public GPG keys from `~/Desktop/Migration/GPG`

      ```sh
      $ gpg --import ~/Desktop/Migration/GPG/public_keys.asc
      ```

   1. Import private GPG keys from `~/Desktop/Migration/GPG`

      ```sh
      $ gpg --import ~/Desktop/Migration/GPG/private_keys.asc
      ```

# Gallery

## Visual Studio Code Samples

![VSCode Sample 1](_images_/vscode-sample-1.png)
![VSCode Sample 2](_images_/vscode-sample-2.png)

## Terminal Samples

![iTerm Sample](_images_/iterm-sample.png)
