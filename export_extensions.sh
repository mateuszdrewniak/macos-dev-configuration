#!/bin/zsh

code --list-extensions > ~/Library/Application\ Support/Code/User/ext.list

echo "Exported"
if type say > /dev/null; then
  say Exported
fi
